# FELIX Dependency Registry

This project contains a cmake file containing the default versions for all projects and dependencies. The values can be overwritten locally bet setting the `<project>_tag` parameter. It can be set to branch names, tags or commit hashes. Setting branch names is discouraged. The repository path (for example for forks) can be changed by setting the `<project>_repo` parameter.

```cmake
set(libfabric_tag "64f2024c22a45a0a0e3d7efd209729264266e549")
set(libfabric_repo "https://gitlab.cern.ch/atlas-tdaq-felix/external-libfabric")
```

## Update a Dependency Version

Set the new tag/hash in the `cmake/versions.cmake` file for the given target.

## Add a New Project

Add the `<project>_tag` and `<project>_repo` variables in the `cmake/versions.cmake` file. Add the package to one of the lists dependending on the project:

* `felix_external_packages` if it is a pre-compiled external package with libraries being installed in the `BINARY_TAG` directory
* `felix_external_packages_no_binary_tag` if it is a pre-compiled external package with libraries being installed in `lib` (no `BINARY_TAG` sub-directory)
* `felix_external_not_precompiled_packages` if it is an external (including TDAQ) but not pre-compiled project
* `felix_internal_packages` if it is a FELIX internal project